import { useState } from "react";
import Contact from "./Contact";

const Counter = ({contacts}) => {

    const [counter, setCounter] = useState( 0 );

    const size = contacts.length -1;

    console.log(size);

    function increment() {
        if( counter !== size ) {
            setCounter( counter + 1 );
        } else {
            setCounter( size );  
        }
    }

    function decrement() {
        if( counter > 0 ) {
            setCounter( counter - 1 );
        } else {
            setCounter( 0 );
        }
        
    }

    function reset() {
        setCounter(0);
    }

    return(
        <>
            <button onClick={ decrement }>-</button>
            <span>{ counter }</span>
            <button onClick={ increment }>+</button>
            <button onClick={reset}>RESET</button>
            {
                contacts.map(( contact, index ) =>(
                    <Contact key={ index }
                        lastname={ contact.lastname }
                        firstname={ contact.firstname }
                        status={ contact.status }
                    />
                ))[counter]
            }
        </>
    );
}
export default Counter;