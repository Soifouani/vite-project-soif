const Contact = (props) => {

    return(
        <>
            <header style={{borderBottom: '1px solid'}}>
                <p>
                    nom: {props.lastname}
                </p>
                <p>
                    prénom: {props.firstname}
                </p>
                <p>
                    status: {props.status === true ? "online" : "offline"}
                </p>
            </header>
        </>
    );
}
export default Contact;