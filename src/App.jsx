import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Counter from './components/Counter'

function App() {

  const contacts = [
    { 
        lastname: 'HAMADA',
        firstname: 'Soifouani',
        status: true,
    },
    { 
        lastname: 'UZUMAKI',
        firstname: 'Naruto',
        status: false,
    },
    { 
        lastname: 'HARUNO',
        firstname: 'Sakura',
        status: true,
    },
    { 
        lastname: 'HYÛGA',
        firstname: 'Neji',
        status: false,
    },
    { 
      lastname: 'UCHIWA',
      firstname: 'Sasuke',
      status: true,
    },
    { 
      lastname: 'HATAKE',
      firstname: 'Kakashi',
      status: false,
    }
]

  return (
    <>
      <Counter contacts = {contacts}/>
    </>
   
    
  )
}

export default App
